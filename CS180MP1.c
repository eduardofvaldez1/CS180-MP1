#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<time.h>
#define MAX_X 40
#define MAX_Y 20

char fileName[]="input.txt";
int grid[MAX_X][MAX_Y], start[2], goal[2], nodeCount; 
clock_t begin, end;
FILE *fp;
FILE *fp2;
FILE *fp3;
struct node {
	int x, y, g, h;
	struct node *link;
	struct node *pred;
	struct node *succ;
};
struct node *FRINGE = NULL, *ROUTE = NULL, *temp;

void printWorld(){
	int i, j;
	
	nodeCount = 0;
	fp2=fopen("output.txt","w");
	
	for (j=MAX_Y-1; j>=0; j--){
		//printf("%d",j%10);
		fprintf(fp2,"%d",j%10);
		for (i=0; i<MAX_X; i++){
			if (grid[i][j] == -3)
			{
				fprintf(fp2,"G");
				//printf("G");
			}
			else if (grid[i][j] == -2){
				fprintf(fp2,"S");
				//printf("S");
				nodeCount++;
			}
			else if (grid[i][j] == -1)
			{
				fprintf(fp2,"X");
				//printf("X");
			}
			else if (grid[i][j] == 1){
				nodeCount++;
				fprintf(fp2,"+");
				//printf("+");
			}
			else if (grid[i][j] == 2){
				nodeCount++;
				fprintf(fp2,"#");
				
				//printf("#");
			
			}
			else
			{
				fprintf(fp2,".");
				//printf(".");
			}
		}
		fprintf(fp2,"\n");
		//printf("\n");
	}
	//printf(" ");
	fprintf(fp2," ");
	for (i=0; i<MAX_X; i++)
	{
		fprintf(fp2,"%d",i%10);
		//printf("%d",i%10);
	}
	fclose(fp2);
}

void addState(struct node **state, struct node **QUEUE, int toRightEnd){
	
	if ((*QUEUE)==NULL){
		(*state)->link = *state;
		*QUEUE=*state;
	}
	else if (toRightEnd==0){
		temp = (*QUEUE)->link;
		(*QUEUE)->link = *state;
		(*state)->link = temp;
		*QUEUE = *state;
	}
	else {
		temp = (*QUEUE)->link;
		(*QUEUE)->link = *state;
		(*state)->link = temp;
	}

	return;
}

int checkState(struct node **QUEUE, int xSuccessor, int ySuccessor){
	
	if (*QUEUE == NULL)
		return 0;
	
	temp=(*QUEUE)->link;
	do {
		if (temp->x == xSuccessor && temp->y == ySuccessor)
			return 1;
		temp = temp->link;
	} while(temp != (*QUEUE)->link);
	return 0;
}

void createSuccessor(int xSuccessor, int ySuccessor, int toRightEnd){
	
	if (xSuccessor >= 0 && xSuccessor < MAX_X && ySuccessor >=0  && ySuccessor < MAX_Y && grid[xSuccessor][ySuccessor] != -1){	//Check if candidate successor is not a wall
		int inQueue=checkState(&FRINGE, xSuccessor, ySuccessor);	//Check if candidate successor is already in FRINGE
		if (inQueue == 0)
			inQueue=checkState(&ROUTE, xSuccessor, ySuccessor);	//Check if candidate successor is already in ROUTE
		if(inQueue == 0){	//Create successor state
			struct node *successor = (struct node *)malloc(sizeof(struct node));
			successor->x = xSuccessor;
			successor->y =  ySuccessor;
			successor->link =  NULL;
			successor->pred =  ROUTE;
			if (toRightEnd == 0)
				addState(&successor,&FRINGE,0);	//Add Successor to end of FRINGE
			else
				addState(&successor,&FRINGE,1);	//Add Successor to beginning of FRINGE
		}
	}
	
	return;
}

void createStarSuccessor(int xSuccessor, int ySuccessor, int g){
	int inQueue = 0;
	struct node *left;
	
	if (xSuccessor >= 0 && xSuccessor < MAX_X && ySuccessor >=0  && ySuccessor < MAX_Y && grid[xSuccessor][ySuccessor] != -1){	//Check if candidate successor is not a wall
		if (abs(goal[0] - xSuccessor) + abs(goal[1] - ySuccessor) > abs(goal[0] - ROUTE->x) + abs(goal[1] - ROUTE->y))
			return;
		//Check if candidate successor is already in FRINGE
		if (FRINGE != NULL){
			temp=FRINGE->link;
			do {
				if (temp->x == xSuccessor && temp->y == ySuccessor){
					if (temp->g  > g){
						temp->g = g;
						temp->pred = ROUTE;
					}
					inQueue =  1;
				}
				temp = temp->link;
			} while(temp != FRINGE->link);
		}
		
		//Check if candidate successor is already in ROUTE
		if (inQueue == 0 && ROUTE != NULL){
			left = ROUTE;
			temp=ROUTE->link;
			do {
				if (temp->x == xSuccessor && temp->y == ySuccessor){
					inQueue =  1;
					if (temp->g  > g){
						grid[temp->x][temp->y] = 0;
						left->link = temp->link;
						free(temp);
						inQueue = 0;
					}
				}
				left = temp;
				temp = temp->link;
			} while(temp != ROUTE->link);
		}
		
		if(inQueue == 0){	//Create successor state
			struct node *successor = (struct node *)malloc(sizeof(struct node));
			successor->x = xSuccessor;
			successor->y =  ySuccessor;
			successor->g =  g;
			successor->h =  abs(goal[0] - successor->x) + abs(goal[1] - successor->y);
			successor->link =  NULL;
			successor->pred =  ROUTE;
			addState(&successor,&FRINGE,0);	//Add Successor to end of FRINGE
		}
	}
	
	return;
}

int BFS(){
	struct node *state;
	int inQueue, forever = 1;

	do {
		if (FRINGE == NULL)	//Check if FRINGE is empty
			return 0;
		
		state=FRINGE->link;
		if (FRINGE->link!=FRINGE){//Remove first State of FRINGE
			FRINGE->link=(FRINGE->link)->link;
			state->link = NULL;
		}
		else
			FRINGE=NULL;
			
		addState(&state,&ROUTE,0);	//Add State to end of ROUTE
		grid[state->x][state->y] = 1;
		
		if (ROUTE->x == goal[0] && ROUTE->y == goal[1])//Check if added State is the Goal State
			return 1;
			
		createSuccessor(ROUTE->x, (ROUTE->y)+1, 0);	//top adjacent grid
		createSuccessor((ROUTE->x)+1, ROUTE->y, 0);	//right adjacent grid
		createSuccessor(ROUTE->x, (ROUTE->y)-1, 0);	//bottom adjacent grid
		createSuccessor((ROUTE->x)-1, ROUTE->y, 0);	//left adjacent grid
	} while (forever == 1);
}

int DFS(){
	struct node *state;
	int inQueue, forever = 1;

	do {
		if (FRINGE == NULL)	//Check if FRINGE is empty
			return 0;
		
		state=FRINGE->link;
		if (FRINGE->link!=FRINGE){//Remove first State of FRINGE
			FRINGE->link=(FRINGE->link)->link;
			state->link = NULL;
		}
		else
			FRINGE=NULL;
			
		addState(&state,&ROUTE,0);	//Add State to end of ROUTE
		grid[state->x][state->y] = 1;
		
		if (ROUTE->x == goal[0] && ROUTE->y == goal[1])//Check if added State is the Goal State
			return 1;

		createSuccessor((ROUTE->x)-1, ROUTE->y, 1);	//left adjacent grid
		createSuccessor(ROUTE->x, (ROUTE->y)-1, 1);	//bottom adjacent grid
		createSuccessor((ROUTE->x)+1, ROUTE->y, 1);	//right adjacent grid
		createSuccessor(ROUTE->x, (ROUTE->y)+1, 1);	//top adjacent grid
	} while (forever == 1);
}

int AStar(){
	struct node *state;
	int inQueue, forever = 1, g=0;

	do {
		if (FRINGE == NULL)	//Check if FRINGE is empty
			return 0;
		
		state=FRINGE->link;
		if (FRINGE->link!=FRINGE){	//Remove shortest State of FRINGE
			temp=state;
			while (temp->link != state){
				temp=temp->link;	
				if (state->g + state->h > temp->g + temp->h)
					state = temp;
			}
			temp->link = state->link;
			state->link = NULL;
		}
		else
			FRINGE=NULL;

		addState(&state,&ROUTE,0);	//Add State to end of ROUTE
		g++;
		grid[state->x][state->y] = 1;
		
		if (ROUTE->x == goal[0] && ROUTE->y == goal[1])//Check if added State is the Goal State
			return 1;

		createStarSuccessor(ROUTE->x, (ROUTE->y)+1, g);	//top adjacent grid
		createStarSuccessor((ROUTE->x)+1, ROUTE->y, g);	//right adjacent grid
		createStarSuccessor(ROUTE->x, (ROUTE->y)-1, g);	//bottom adjacent grid
		createStarSuccessor((ROUTE->x)-1, ROUTE->y, g);	//left adjacent grid
	} while (forever == 1);
}

int main(){
	char c, algo, newline;
	int p, v, i, j, xLow, xHigh, yLow, yHigh, x, y, noVertex = 0, vertexCount=1, vertexCounter=0, polygonCount=0, scanned, cost=0, xDelta, yDelta, solution;
	float m, b;
	
	printf("\nPROGRAM STARTED\n");
	
	printf("\nChoose a Search Algorithm\n\ta) Breadth First Search\n\tb) Depth First Search\n\tc) A* Search\n\tx) Exit\n");
	do {
		printf("Choice: ");
		scanned=scanf("%c%c", &algo, &newline);
		if (algo == 'a' && newline == '\n' && scanned ==2){}
		else if (algo == 'b' && newline == '\n' && scanned ==2){}
		else if (algo == 'c' && newline == '\n' && scanned ==2){}
		else if (algo == 'x' && newline == '\n' && scanned ==2){
			printf("\nPROGRAM TERMINATED\n");
			return 0;
		}
		else
			printf("\nInvalid choice. Choose again.\n");
		fflush(stdin);
	} while ((algo != 'a' && algo != 'b' && algo != 'c' && algo != 'x') || newline != '\n' || scanned !=2);
		
	fp=fopen(fileName, "r");
	while ((c = fgetc(fp))!= EOF){
		if (c == ',' && polygonCount != 0)
			vertexCounter++;
		if (c == '\n'){
			polygonCount++;
			if (vertexCounter > vertexCount)
				vertexCount = vertexCounter;
			vertexCounter = 0;
		}
	}
	if (polygonCount == 0){
		polygonCount = 1;
		noVertex = 1;
	}
	if (vertexCounter > vertexCount)
		vertexCount = vertexCounter;
	fclose(fp);

	int polygon[polygonCount][vertexCount][3];
	for (p=0; p<polygonCount; p++){
		for (v=0; v<vertexCount; v++){
			polygon[p][v][2]=0;
		}
	}
	fp=fopen(fileName, "r");
	for (i=0; i<2; i++){
		start[i] = fgetc(fp)-48;
		while ((c=fgetc(fp)) != ',' && c != '\t')
			start[i] = start[i]*10 + c-48;
	}
	for (i=0; i<2; i++){
		goal[i] = fgetc(fp)-48;
		while ((c=fgetc(fp)) != ',' && c != '\n' && c!= EOF)
			goal[i] = goal[i]*10 + c-48;
	}
	
	for (p=0; p<polygonCount; p++){
		for (v=0; v<vertexCount ; v++){
			for (i=0; i<2; i++){
				polygon[p][v][i] = fgetc(fp)-48;
				while ((c=fgetc(fp)) != ',' && c != '\t' && c != '\n' && c != EOF){
					polygon[p][v][i] = polygon[p][v][i]*10 + c-48;
				}
			}
			if (c == '\n' || c == EOF)
				break;
		}
		for (i=0; i<=v; i++)
			polygon[p][i][2] = v;
	}
	fclose(fp);
	if (noVertex != 1){
		for (p=0; p<polygonCount; p++){
			for (v=0; v<=polygon[p][0][2]; v++){
				for (i=0; i<=polygon[p][0][2]-v; i++){
					if (polygon[p][v][0] < polygon[p][v+i][0] || i==0){
						xLow = polygon[p][v][0];
						xHigh = polygon[p][v+i][0];
					}
					else if (polygon[p][v][0] > polygon[p][v+i][0]){
						xLow = polygon[p][v+i][0];
						xHigh = polygon[p][v][0];
					}
					if (polygon[p][v][1] < polygon[p][v+i][1] || i==0){
						yLow = polygon[p][v][1];
						yHigh = polygon[p][v+i][1];
					}
					else if (polygon[p][v][1] > polygon[p][v+i][1]){
						yLow = polygon[p][v+i][1];
						yHigh = polygon[p][v][1];
					}
					if (polygon[p][v][0] == polygon[p][v+i][0]){
						for (y=yLow; y<=yHigh; y++)
							grid[polygon[p][v][0]][y] = -1;
					}
					else if (polygon[p][v][1] == polygon[p][v+i][1]){
						for (x=xLow; x<=xHigh; x++)
							grid[x][polygon[p][v][1]] = -1;
					}
					else {
						yDelta = (polygon[p][v][1] - polygon[p][v+i][1]);
						xDelta = (polygon[p][v][0] - polygon[p][v+i][0]);
						m = (float) yDelta/(float) xDelta;
						b = polygon[p][v][1] - m*polygon[p][v][0];
						if (abs( (int) m)<1){
							for (x=xLow; x<=xHigh; x++)
									grid[x][(int)(m*((float)x) + b)] = -1;
						}
						else {
							for (y=yLow; y<=yHigh; y++){
								if (polygon[p][v][2]==1)
									grid[(int)round(((float)y-b)/m)][y] = -1;
								else {
									if (m>0)
										grid[(int)floor(((float)y-b)/m)][y] = -1;
									else
										grid[(int)ceil(((float)y-b)/m)][y] = -1;
								}
							}
						}
					}
				}
			}
		}
	}
	
	
	struct node *startState = (struct node *)malloc(sizeof(struct node));
	startState->x=start[0];
	startState->y=start[1];
	startState->g=0;
	startState->h= abs(goal[0] - startState->x) + abs(goal[1] - startState->y);
	startState->link=NULL;
	startState->pred=NULL;
	addState(&startState, &FRINGE, 0);
	
	grid[start[0]][start[1]] = -2;
	grid[goal[0]][goal[1]] = -3;

	begin = clock();
	if (algo == 'a')
		solution=BFS();
	else if (algo == 'b')
		solution=DFS();
	else if (algo == 'c')
		solution=AStar();
	end = clock();

	if (solution == 1){
		temp = ROUTE;
		while (temp->x != start[0] || temp->y != start[1]){
			grid[temp->x][temp->y] = 2;
			cost++;
			(temp->pred)->succ=temp;
			temp=temp->pred;
		}

		fp3=fopen("path.txt","w");
		while (temp!=NULL){
				fprintf(fp3,"(%d,%d)\n",temp->x,temp->y);
			temp=temp->succ;
		}
		fclose(fp3);
	}

	grid[start[0]][start[1]] = -2;
	grid[goal[0]][goal[1]] = -3;
	printWorld();
	

	if (algo == 'a')
		printf("\nBreadth First Search");
	else if (algo == 'b')
		printf("\nDepth First Search");
	else if (algo == 'c')
		printf("\nA* Search");
	if (solution==0)
		printf("\nStarting Point: (%d, %d)\nGoal Point: (%d, %d)\nNumber of Expanded Nodes: %d\nCost of the Solution: No Solution\nActual Running Time: %f\n\nPROGRAM ENDED\n", start[0], start[1], goal[0], goal[1], nodeCount, (float)(end - begin)/CLOCKS_PER_SEC);
	else
		printf("Starting Point: (%d, %d)\nGoal Point: (%d, %d)\nNumber of Expanded Nodes: %d\nCost of the Solution: %d\nActual Running Time: %f\n\nPROGRAM ENDED\n", start[0], start[1], goal[0], goal[1], nodeCount, cost, (float)(end - begin)/CLOCKS_PER_SEC);

	return 0;
}
